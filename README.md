# Trending Hashtags

## Intro

This is the RiteKit Android Developer Internship Chanllenge repository. This application uses RiteKit API to get trending hashtags, show statistics about them, and influencers. It contains two screens:

- **Main Screen:** Here the user can see the list of the latest trending hashtags, and click on the one that interests him/her to see more details about it.
- **Detail Screen:** Here the user can see statistics about the selected hashtag, as well as a list of influencers using it. The user can click on any of the influencers' cards to open their Twitter profile outside of the application.

The web requests are implemented using `Retrofit` library. To keep things simple, the project was structured in the following manner:

- **`adapter` package folder:** This folder contains Adapter classes that map data obtained from the API to their respective views in `RecyclerView` lists and grids.
- **`model` package folder:** Contains simple classes that represent the schema of the json data retrieved from different endpoints of the API. `Retrofit` uses these classes to abstract dealing with json responses for the developer.
- **`network` package folder:** This folder contains `RetrofitInstance` class that returns a single instance of the `Retrofit` object (because we are using one API only with the library). It also contains two interfaces that represent network calls to different endpoints of the server. The calls are triggered using methods and can take different parameter values, like `client_id` for example.
- **`ui` package folder:** This folder contains `Activity` classes, objects that are displayed on the screen.

The application has a backward support back until Android 4.4. It was tested using my Samsgung Galaxy S6 Edge, Android version 7.0.

## Handling Network Errors

I made use of the `onFailure` callback that comes with `Retrofit`. If the request fails, a snackbar message shows at the bottom of the screen notifying the user and giving him/her the option to retry over and over until the request succeeds. Also, I made sure there is a spinning progress bar to inform the user that the request is being loaded, which is better than a blank screen. This way, I made sure that user knows when something is wrong with their network while at the same time giving them the option to retry the request without the need of restarting the app. See the screenshots:

![main_offline](screenshots/main_offline.png)

![main_online](screenshots/main_online.png)

![detail_offline](screenshots/detail_offline.png)

![detail_online](screenshots/detail_online.png)

## A Little Challenge

Whenever a screen is rotated, a new network request was made, which is not efficient at all. The reason behind it is tied to the Android Activity Lifecycle. I had to save the state of the loaded data once it is loaded so that the loading job is done only once. To do that, I had to implement `Parcelable` interface on both `Hashtag` and `Influencer` classes, store the loaded data in the `instanceState` by overriding `onSaveInstanceState` method, and attempt to get them in `OnCreate` method. If data is not found, the application makes a network request, stores the data, and displays it on the screen. If the data is already there, the application bypasses the network request and displays the data it has. This way, the application makes less network requests and the application can use the data it got the first time even if connectivity is lost. Using `Parcelable` interface was a little tricky, but it was rewarding at the end as I was able to send `Hashtag` objects to `DetailActivity` instead of sending every piece of information separately.