package io.github.iskandergaba.trendinghashtags.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HashtagList {

    @SerializedName("result")
    private boolean result;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;
    @SerializedName("tags")
    private ArrayList<Hashtag> hashtagList;

    public boolean isResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public ArrayList<Hashtag> getHashtagList() {
        return hashtagList;
    }
}
