package io.github.iskandergaba.trendinghashtags.ui;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;

import io.github.iskandergaba.trendinghashtags.R;
import io.github.iskandergaba.trendinghashtags.adapter.HashtagAdapter;
import io.github.iskandergaba.trendinghashtags.model.Hashtag;
import io.github.iskandergaba.trendinghashtags.model.HashtagList;
import io.github.iskandergaba.trendinghashtags.network.GetHashtagDataService;
import io.github.iskandergaba.trendinghashtags.network.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private HashtagAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ArrayList<Hashtag> hashtags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("hashtags")) {
            hashtags = savedInstanceState.getParcelableArrayList("hashtags");
        }
        setContentView(R.layout.activity_main);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(MainActivity.this);
        recyclerView = findViewById(R.id.recycler_view_hashtag_list);
        recyclerView.setLayoutManager(layoutManager);
        progressBar = findViewById(R.id.progress_bar);
        final GetHashtagDataService service = RetrofitInstance.getRetrofitInstance()
                .create(GetHashtagDataService.class);
        getHashtags(service);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("hashtags", hashtags);
    }

    private void getHashtags(final GetHashtagDataService service) {
        if (hashtags == null) {
            progressBar.setVisibility(View.VISIBLE);
            Call<HashtagList> call = service.getHashtagData(1, 1, getString(R.string.client_id));
            Log.v("URL Called", call.request().url().toString());
            call.enqueue(new Callback<HashtagList>() {
                @Override
                public void onResponse(@NonNull Call<HashtagList> call,
                                       @NonNull Response<HashtagList> response) {
                    if (response.body() != null) {
                        hashtags = response.body().getHashtagList();
                        progressBar.setVisibility(View.INVISIBLE);
                        adapter = new HashtagAdapter(MainActivity.this, hashtags);
                        recyclerView.setAdapter(adapter);
                    }
                }
                @Override
                public void onFailure(@NonNull Call<HashtagList> call, @NonNull Throwable t) {
                    showErrorSnackbar(service);
                }
            });
        } else {
            adapter = new HashtagAdapter(MainActivity.this, hashtags);
            recyclerView.setAdapter(adapter);
        }
    }

    private void showErrorSnackbar(final GetHashtagDataService service) {
        Snackbar errorSnackbar = Snackbar.make(findViewById(R.id.activity_main),
                R.string.msg_network_error, Snackbar.LENGTH_INDEFINITE);
        errorSnackbar.setAction(R.string.button_network_error_retry, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHashtags(service);
            }
        });
        errorSnackbar.show();
    }
}
