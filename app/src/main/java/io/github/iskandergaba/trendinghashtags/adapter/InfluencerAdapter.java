package io.github.iskandergaba.trendinghashtags.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import io.github.iskandergaba.trendinghashtags.R;
import io.github.iskandergaba.trendinghashtags.model.Influencer;

public class InfluencerAdapter
        extends RecyclerView.Adapter<InfluencerAdapter.InfluencerViewHolder> {

    private Context context;
    private List<Influencer> influencerList;

    public InfluencerAdapter(Context context, List<Influencer> influencerList) {
        this.context = context;
        this.influencerList = influencerList;
    }

    @NonNull
    @Override
    public InfluencerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.grid_item_influencer, parent, false);
        return new InfluencerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InfluencerViewHolder holder, int position) {
        holder.setInfluencer(influencerList.get(position));
    }

    @Override
    public int getItemCount() {
        return influencerList.size();
    }

    class InfluencerViewHolder extends RecyclerView.ViewHolder {

        ImageView imageProfilePhoto;
        TextView textUsername, textFollowers;
        Influencer influencer;

        InfluencerViewHolder(View itemView) {
            super(itemView);
            imageProfilePhoto = itemView.findViewById(R.id.profile_photo);
            textUsername = itemView.findViewById(R.id.username);
            textFollowers = itemView.findViewById(R.id.followers);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(String.format("https://twitter.com/%s", influencer.getUsername())));
                    context.startActivity(intent);
                }
            });
        }

        void setInfluencer(Influencer influencer) {
            this.influencer = influencer;
            Glide.with(context).load(influencer.getPhoto()).into(imageProfilePhoto);
            textUsername.setText(String.format("@%s", influencer.getUsername()));
            textFollowers.setText(String.format(Locale.getDefault(), "%d followers", influencer.getFollowers()));

        }
    }
}
