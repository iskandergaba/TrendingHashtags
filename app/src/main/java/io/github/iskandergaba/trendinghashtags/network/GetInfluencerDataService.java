package io.github.iskandergaba.trendinghashtags.network;

import io.github.iskandergaba.trendinghashtags.model.HashtagList;
import io.github.iskandergaba.trendinghashtags.model.InfluencerList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetInfluencerDataService {
    @GET("influencers/hashtag/{tag}")
    Call<InfluencerList> getInfluencerData(@Path("tag") String hashtag,
                                           @Query("client_id") String id);
}
