package io.github.iskandergaba.trendinghashtags.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Influencer implements Parcelable {

    @SerializedName("username")
    private String username;
    @SerializedName("photo")
    private String photo;
    @SerializedName("followers")
    private int followers;

    public String getUsername() {
        return username;
    }

    public String getPhoto() {
        return photo;
    }

    public int getFollowers() {
        return followers;
    }

    private Influencer(Parcel in) {
        username = in.readString();
        photo = in.readString();
        followers = in.readInt();
    }

    public static final Creator<Influencer> CREATOR = new Creator<Influencer>() {
        @Override
        public Influencer createFromParcel(Parcel in) {
            return new Influencer(in);
        }

        @Override
        public Influencer[] newArray(int size) {
            return new Influencer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(photo);
        parcel.writeInt(followers);
    }
}
