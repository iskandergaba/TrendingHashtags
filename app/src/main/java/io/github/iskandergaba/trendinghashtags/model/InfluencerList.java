package io.github.iskandergaba.trendinghashtags.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InfluencerList {

    @SerializedName("result")
    private boolean result;
    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("hashtag")
    private String hashtag;
    @SerializedName("influencers")
    private ArrayList<Influencer> influencerList;

    public boolean isResult() {
        return result;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getHashtag() {
        return hashtag;
    }

    public ArrayList<Influencer> getInfluencerList() {
        return influencerList;
    }
}
