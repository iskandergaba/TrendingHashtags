package io.github.iskandergaba.trendinghashtags.network;

import io.github.iskandergaba.trendinghashtags.model.HashtagList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetHashtagDataService {
    @GET("search/trending")
    Call<HashtagList> getHashtagData(@Query("green") int green,
                                     @Query("latin") int latin,
                                     @Query("client_id") String id);
}
