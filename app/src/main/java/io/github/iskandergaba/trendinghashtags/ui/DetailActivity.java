package io.github.iskandergaba.trendinghashtags.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import io.github.iskandergaba.trendinghashtags.R;
import io.github.iskandergaba.trendinghashtags.adapter.InfluencerAdapter;
import io.github.iskandergaba.trendinghashtags.model.Hashtag;
import io.github.iskandergaba.trendinghashtags.model.Influencer;
import io.github.iskandergaba.trendinghashtags.model.InfluencerList;
import io.github.iskandergaba.trendinghashtags.network.GetInfluencerDataService;
import io.github.iskandergaba.trendinghashtags.network.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    private InfluencerAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ArrayList<Influencer> influencers;
    private Hashtag hashtag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("influencers")) {
            influencers = savedInstanceState.getParcelableArrayList("influencers");
        }
        setContentView(R.layout.activity_detail);
        hashtag = getIntent().getParcelableExtra("hashtag");
        setTitle(String.format("#%s", hashtag.getTag()));
        TextView textTweets = findViewById(R.id.tweets);
        textTweets.setText(String.format(Locale.getDefault(), "Tweets: %d", hashtag.getTweets()));
        TextView textRetweets = findViewById(R.id.retweets);
        textRetweets.setText(String.format(Locale.getDefault(), "Retweets: %d", hashtag.getRetweets()));
        TextView textExposure = findViewById(R.id.exposure);
        textExposure.setText(String.format(Locale.getDefault(), "Exposure: %d", hashtag.getExposure()));
        TextView textLinks = findViewById(R.id.links);
        textLinks.setText(String.format(Locale.getDefault(), "Links: %f", hashtag.getLinks()));
        TextView textPhotos = findViewById(R.id.photos);
        textPhotos.setText(String.format(Locale.getDefault(), "Photos: %f", hashtag.getPhotos()));
        TextView textMentions = findViewById(R.id.mentions);
        textMentions.setText(String.format(Locale.getDefault(), "Mentions: %f", hashtag.getMentions()));

        RecyclerView.LayoutManager layoutManager =
                new GridLayoutManager(DetailActivity.this, 2);
        recyclerView = findViewById(R.id.recycler_view_influencer_list);
        recyclerView.setLayoutManager(layoutManager);
        progressBar = findViewById(R.id.progress_bar);
        final GetInfluencerDataService service = RetrofitInstance.getRetrofitInstance()
                .create(GetInfluencerDataService.class);
        getInfluencers(service);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("influencers", influencers);
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    private void getInfluencers(final GetInfluencerDataService service) {
        if (influencers == null) {
            progressBar.setVisibility(View.VISIBLE);
            Call<InfluencerList> call = service.getInfluencerData(hashtag.getTag(),
                    getString(R.string.client_id));
            Log.v("URL Called", call.request().url().toString());
            call.enqueue(new Callback<InfluencerList>() {
                @Override
                public void onResponse(@NonNull Call<InfluencerList> call,
                                       @NonNull Response<InfluencerList> response) {
                    if (response.body() != null) {
                        influencers = response.body().getInfluencerList();
                        progressBar.setVisibility(View.INVISIBLE);
                        adapter = new InfluencerAdapter(DetailActivity.this, influencers);
                        recyclerView.setAdapter(adapter);
                    }
                }
                @Override
                public void onFailure(@NonNull Call<InfluencerList> call, @NonNull Throwable t) {
                    showErrorSnackbar(service);
                }
            });
        } else {
            adapter = new InfluencerAdapter(DetailActivity.this, influencers);
            recyclerView.setAdapter(adapter);
        }
    }

    private void showErrorSnackbar(final GetInfluencerDataService service) {
        Snackbar errorSnackbar = Snackbar.make(findViewById(R.id.activity_detail),
                R.string.msg_network_error, Snackbar.LENGTH_INDEFINITE);
        errorSnackbar.setAction(R.string.button_network_error_retry, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInfluencers(service);
            }
        });
        errorSnackbar.show();
    }
}
