package io.github.iskandergaba.trendinghashtags.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import io.github.iskandergaba.trendinghashtags.R;
import io.github.iskandergaba.trendinghashtags.model.Hashtag;
import io.github.iskandergaba.trendinghashtags.ui.DetailActivity;

public class HashtagAdapter extends RecyclerView.Adapter<HashtagAdapter.HashtagViewHolder> {

    private Context context;
    private List<Hashtag> hashtagList;

    public HashtagAdapter(Context context, List<Hashtag> hashtagList) {
        this.hashtagList = hashtagList;
        this.context = context;
    }

    @NonNull
    @Override
    public HashtagViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_hashtag, parent, false);
        return new HashtagViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HashtagViewHolder holder, int position) {
        Hashtag hashtag = hashtagList.get(position);
        holder.setHashtag(hashtag);
   }

    @Override
    public int getItemCount() {
        return hashtagList.size();
    }

    class HashtagViewHolder extends RecyclerView.ViewHolder {

        TextView textHashtag, textTweets, textRetweets, textExposure;
        Hashtag hashtag;

        HashtagViewHolder(View itemView) {
            super(itemView);
            textHashtag = itemView.findViewById(R.id.hashtag);
            textTweets = itemView.findViewById(R.id.tweets);
            textRetweets = itemView.findViewById(R.id.retweets);
            textExposure = itemView.findViewById(R.id.exposure);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("hashtag", hashtag);
                    context.startActivity(intent);
                }
            });
        }

        void setHashtag(Hashtag hashtag) {
            this.hashtag = hashtag;
            textHashtag.setText(String.format(Locale.getDefault(), "#%s", hashtag.getTag()));
            textTweets.setText(String.format(Locale.getDefault(), "Tweets: %d", hashtag.getTweets()));
            textRetweets.setText(String.format(Locale.getDefault(), "Retweets: %d", hashtag.getRetweets()));
            textExposure.setText(String.format(Locale.getDefault(), "Exposure: %d", hashtag.getExposure()));

        }
    }
}
