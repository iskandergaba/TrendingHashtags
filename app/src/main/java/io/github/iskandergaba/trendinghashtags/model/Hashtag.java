package io.github.iskandergaba.trendinghashtags.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Hashtag implements Parcelable {

    @SerializedName("tag")
    private String tag;
    @SerializedName("tweets")
    private int tweets;
    @SerializedName("retweets")
    private int retweets;
    @SerializedName("exposure")
    private int exposure;
    @SerializedName("links")
    private double links;
    @SerializedName("photos")
    private double photos;
    @SerializedName("mentions")
    private double mentions;
    @SerializedName("color")
    private int color;

    public String getTag() {
        return tag;
    }

    public int getTweets() {
        return tweets;
    }

    public int getRetweets() {
        return retweets;
    }

    public int getExposure() {
        return exposure;
    }

    public double getLinks() {
        return links;
    }

    public double getPhotos() {
        return photos;
    }

    public double getMentions() {
        return mentions;
    }

    public int getColor() {
        return color;
    }

    private Hashtag(Parcel in) {
        tag = in.readString();
        tweets = in.readInt();
        retweets = in.readInt();
        exposure = in.readInt();
        links = in.readDouble();
        photos = in.readDouble();
        mentions = in.readDouble();
        color = in.readInt();
    }

    public static final Creator<Hashtag> CREATOR = new Creator<Hashtag>() {
        @Override
        public Hashtag createFromParcel(Parcel in) {
            return new Hashtag(in);
        }

        @Override
        public Hashtag[] newArray(int size) {
            return new Hashtag[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tag);
        parcel.writeInt(tweets);
        parcel.writeInt(retweets);
        parcel.writeInt(exposure);
        parcel.writeDouble(links);
        parcel.writeDouble(photos);
        parcel.writeDouble(mentions);
        parcel.writeInt(color);
    }
}
